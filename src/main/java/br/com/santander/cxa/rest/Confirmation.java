package br.com.santander.cxa.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.cxa.model.ConfirmationRequest;
import br.com.santander.cxa.model.ConfirmationResponse;

@RestController
@RequestMapping("/CXA")
public class Confirmation {
	
	@PostMapping("/confirmation")
	public ConfirmationResponse confirmation(@RequestBody ConfirmationRequest request) {
		return new ConfirmationResponse();
	}

}
