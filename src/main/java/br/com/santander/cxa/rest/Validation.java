package br.com.santander.cxa.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.cxa.model.ValidationRequest;
import br.com.santander.cxa.model.ValidationResponse;

@RestController
@RequestMapping("/CXA/validation")
public class Validation {
	
	@PostMapping
	public ValidationResponse confirmation(@RequestBody ValidationRequest request) {
		return new ValidationResponse();
	}

}
