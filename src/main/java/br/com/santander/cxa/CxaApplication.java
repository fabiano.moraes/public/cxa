package br.com.santander.cxa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CxaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CxaApplication.class, args);
	}

}
