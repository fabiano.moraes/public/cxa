package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiptsDataDTO {

	private String rcptInfo;
	private String rcptTxt;
	private String rcptAuthenticationFlag;
	private String rcptAuthenticationTxt;

}
