package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerServiceDataDTO {

	private String bankCode;
	private String branchCode;
	private String customerServiceNSU;
	private String operatorEnrollment;
	private String operatorCode;
	private String terminalCode;
	private String customerServiceStatus;
	private String creationDate;
	private String accountingDate;
	private String emailFlag;
	private String totalValue;
	private String paymentType;
	private String paymentValue;
	private String cashValue;
	private String withdrawalValue;
	private String changeMoneyFlag;
	private String changeMoneyValue;
	private String loadType;
	private String loadValue;
	private String txPgInddup;
	private CustomerDataDTO customerData;
	private ServiceTransactionDataDTO serviceTransactionData;
	private PldDataDTO pldData;
	private ProductDataDTO productData;
	private CustomerAuthDataDTO customerAuthData;

}
