package br.com.santander.cxa.model;

import br.com.santander.cxa.model.dto.DocumentAuthenticationDataDTO;
import br.com.santander.cxa.model.dto.ReceiptsDataDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmationResponse {

	private String excecutionCode;
	private String executionMessage;
	private String imperativeAuthRemoteFlag;
	private String imperativeAuthRemoteData;
	private String reExecutionCustomerAuthFlag;
	private String reExecutionData;
	private ReceiptsDataDTO receiptsData;
	private DocumentAuthenticationDataDTO documentAuthentication;

}
