package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BioDataDTO {

	private String electronicSign;
	private String operationCounter;
	private String dna;

}
