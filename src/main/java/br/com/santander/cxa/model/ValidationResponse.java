package br.com.santander.cxa.model;

import br.com.santander.cxa.model.dto.CustomerDataDTO;
import br.com.santander.cxa.model.dto.ErrorDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ValidationResponse {

	
	private String bankCode;

	
	private String branchCode;

	
	private String operatorEnrollment;

	
	private String terminalCode;

	
	private String accountingDate;

	
	private String customerServiceNSU;

	
	private String operatorAuthCode;

	
	private String transactionNSU;

	
	private String authReasonMsg;

	
	private String serviceID;

	
	private String serviceType;

	
	private String customerAuthFlag;

	
	private String visibleFlag;

	
	private String pldFlag;

	
	private String serviceDate;

	
	private String operationValue;

	
	private String productData;

	private CustomerDataDTO customerData;
	
	private ErrorDTO error;
}
