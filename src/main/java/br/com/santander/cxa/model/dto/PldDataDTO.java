package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PldDataDTO {

	private String customerServiceNSU;
	private String transactionNSU;
	private String transactionType;
	private String accountingDate;
	private String operatorBranch;
	private String customerName;
	private String customerType;
	private String customerDocValue;
	private String customerResourcesMsg;
	private String customerDocType;
	private String customerForeignerDocument;
	private String customerDocCountry;
	private String customerInternationalOrg;
	private String orgName;
	private String reversalNsu;

}
