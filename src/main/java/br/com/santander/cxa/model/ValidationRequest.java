package br.com.santander.cxa.model;

import br.com.santander.cxa.model.dto.ContextDataDTO;
import br.com.santander.cxa.model.dto.CustomerDataDTO;
import br.com.santander.cxa.model.dto.GareDataDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationRequest {

	private ContextDataDTO context;
	
	private CustomerDataDTO customer;
	
	private GareDataDTO gare;
}
