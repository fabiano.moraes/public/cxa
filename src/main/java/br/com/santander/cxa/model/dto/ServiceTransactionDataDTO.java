package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceTransactionDataDTO {

	private String customerServiceNSU;
	private String transactionNSU;
	private String serviceID;
	private String serviceStatus;
	private String serviceDate;
	private String chargebacklFlag;
	private String operationValue;
	private String customerAuthStatus;
	private String customerAuthType;
	private String fepFlag;
	private String remoteAuthStatus;
	private String pldStatus;
	private String authReasonMsg;
	private String approverEnrollment1;
	private String approverEnrollment2;
	private String approverEnrollment3;
}
