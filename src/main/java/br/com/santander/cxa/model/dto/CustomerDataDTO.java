package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDataDTO {

	private String name;
	private String penumper;
	private String customerFlag;
	private String customerType;
	private String cpfCnpj;
	private String bankCode;
	private String branchCode;
	private String bankAccount;
	private String accountType;
	private String email;
	private String cardNumber;
}
