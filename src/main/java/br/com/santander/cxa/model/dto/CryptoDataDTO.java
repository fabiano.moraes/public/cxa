package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CryptoDataDTO {

	private String bankCode;
	private String branchCode;
	private String nsu;
	private String penumper;
	private String bankAccount;
	private String accountType;
	private String fiscalType;
	private String document;
	private String biometricFlag;
}
