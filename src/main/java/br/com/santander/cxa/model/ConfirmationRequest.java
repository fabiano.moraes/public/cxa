package br.com.santander.cxa.model;

import br.com.santander.cxa.model.dto.CustomerServiceDataDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmationRequest {

	private CustomerServiceDataDTO customerServiceData;

}
