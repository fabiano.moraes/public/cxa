package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContextDataDTO {

	private String bank;
	
	
	private String branch;
	
	
	private String accountingDate;
	
	
	private String terminal;
	
	
	private String dependency;
	
	
	private String operator;
	
	
	private String registration;
	
	
	private String supervisor;
	
	
	private String attendanceNsu;
	
	
	private String authenticationNumber;
	
	
	private String networkUserCode;

	
	private String hostname;
	
	
	private String ipAddress;
	
	
	private String selectedMode;
}
