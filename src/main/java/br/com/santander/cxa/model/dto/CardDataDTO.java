package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardDataDTO {

	private String cardNumber;
	private String cardTrack;
	private String cardExpirationDate;

}
