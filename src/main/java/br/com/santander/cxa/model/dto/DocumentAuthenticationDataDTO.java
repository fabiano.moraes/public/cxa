package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocumentAuthenticationDataDTO {

	private String docInfo;
	private String docAuthenticationTxt;

}
