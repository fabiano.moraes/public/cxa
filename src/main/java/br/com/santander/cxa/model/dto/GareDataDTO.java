package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GareDataDTO {
	private String dataDeVencimento;
	private String codigoReceita;
	private String camposVar4;
	private String camposVar5;
	private String camposVar6;
	private String camposVar7;
	private String camposVar8;
	private String valorPrincipal;
	private String valorDosJuros;
	private String valorDaMulta;
	private String valorDosAcrescimos;
	private String valorDosHonorarios;
	private String valorDaPostagem;
}
