package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDataDTO {

	private String nsu;
	private String networkUserCode;
	private String registration;
	private String authenticationNumber;
	private String transactionKey;
	private String dependencyCode;
	private String hostname;
	private String ipAddress;
	private String selectedMode;
	private String digitVerifier;
	private String digitQuantity;
	
	//Campos do GARE
	private String dataDeVencimento;
	private String codigoReceita;
	private String camposVar4;
	private String camposVar5;
	private String camposVar6;
	private String camposVar7;
	private String camposVar8;
	private String valorPrincipal;
	private String valorDosJuros;
	private String valorDaMulta;
	private String valorDosAcrescimos;
	private String valorDosHonorarios;
	private String valorDaPostagem;

	
}
