package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerAuthDataDTO {

	private CryptoDataDTO cryptoData;
	private CryptopanDTO cryptopan;
	private CardDataDTO cardData;
	private BioDataDTO bioData;
}
