package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDTO {

	private String timestamp;

	private Integer httpStatusCode;

	private Object details;

	private String trackingId;
}