package br.com.santander.cxa.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CryptopanDTO {

	private String fullPan;
	private String pan;
	private String panCont;

}
